format()
==================================
result = 100/777
print(result)
print("The result was {r:1.5f}".format(r = result))

name = 		"Jose"
print("Hello , His name is {}".format(name))
print(f"Hello , His name is {name}")

name = "Sam"
age = 3
print(f"{name} is {age} years old.")




Lists
==============================

- Lists are ordered sequence that can hold a variety of objects types.
- Use [] and commas to separate objects in List e.g., [1,2,3,4,5]
- Lists support indexing and slicing.
- Lists can be nested and also have a variety of useful methods



my_list = [1,2,3]
my_list = ["String" , 1000 , 23.2]
print(my_list)
print(len(my_list))

list1 = ["one" , "two" , "three"]
print(list1)
print(list1[1:])
list2 = ["four" , "five"]
print(list1 + list2)

new_list = list1 + list2
print(new_list)

new_list[0] = "ONE ALL CAPS"
print(new_list)

new_list.append("six")
print(new_list)
new_list.append("seven")
print(new_list)

print(new_list.pop())
print(new_list)

popped_item = new_list.pop();
print(popped_item)
print(new_list)

print(new_list.pop(0))
print(new_list)

new_list = ['a' , 'x' , 'e' , 'b' , 'c' , 'd']
num_list = [4,1,3,8]

print("Before Sort ",new_list)
# print(new_list.sort()) #will return nothing, will just sort the list
new_list.sort()
print("After Sort  ",new_list)

# OR

sorted_list = new_list.sort()
print(sorted_list) #will give None as it will return nothing
# So do this

new_list.sort()
sorted_list = new_list
print(sorted_list)

new_list.reverse()
print(new_list)




Dictionaries
===========================

- Dictionaries are unordered mapping for storing objects
- Use key-value pairing 
- syntax : {"key":"value" , "key":"value"}

Difference between Dictionaries and list
==================================================
Dictionaries - Objects retrieved by key name
			 - Can not be sorted
			 - Mutable

Lists - Objects retrieved by location
	  - Ordered sequence can be indexed and sliced	
	  - Mutable		 

Tuples - Immutable

dict1 = {"key1":"value1" , "key2":"value2"}
print(dict1)
print(len(dict1))
print(type(dict1))
print(dict1["key1"])


prices = {"apple":2.99 , "guava":3.99 , "watermelon":10 , "milk":20}
print(prices)
print(prices["apple"])

d = {"k1":123 , "k2":[3,4,5] , "k3":{"insidekey":100}} #nested list , dictionary
print(d)
print(d["k2"])
print(d["k2"][1])
print(d["k3"])
print(d["k3"]["insidekey"])
print(d["k1"])

d = {"k1":[1,2,3,4,"a"]}
print(d)
list1 = d["k1"]
print(list1)
print(list1[1])
print(list1[4].upper())

print(d["k1"][4].upper()) #access element


d = {"k1":100 , "k2":200} #Add element to Dictionary
print(d)
d["k3"] = 300
print(d)
d["k1"] = "One Value"
print(d)

print(d.keys())
print(d.values())
print(d.items())




Tuples
====================

- Very similar to lists
- It is Immutable , value cannot be re-assigned
- e.g, (1,2,3,4)


t = (1,2,3)
list1 = [1,2,3]

print(type(t))
print(type(list1))

print(t)
print(list1)

t = ('one', 2)
print(t)
print(t[0])
# t[0] = "Two"  #value can not be re-assigned
print(t[-1])

t = ("a" , "a" , "b")
print(t)
print(t.count("a")) #count how many times "a" is there
print(t.index("a")) #returns where it will get the value first
print(t.index("b"))





Sets
===================

- Unordered collection of unique objects
- Duplicates are not allowed

set1 = set()
print(set1)
set1.add(1)
set1.add(2)
set1.add(3)
set1.add(4)
print(set1)

list1 = [1,1,1,1,2,2,2,3,3,3,3,4,4,4,5,5,5,5,5,5,5]
set2 = set(list1)
print(set2)


#OR

set3 = {1,2,3,4,4,5,6,7}
print(set3)

var = "Mississippi"
set4 = set(var)
print(set4)

#OR

print(set("Mississippi"))




Booleans
=========================
- Convey true or false

print(True)
print("True")
print(type(True))

print(1 > 2)
print(2 > 1)




Basic Practice:

http://codingbat.com/python

More Mathematical (and Harder) Practice:

https://projecteuler.net/archives

List of Practice Problems:

http://www.codeabbey.com/index/task_list

A SubReddit Devoted to Daily Practice Problems:

https://www.reddit.com/r/dailyprogrammer




print(1+2.3+3)
print()
print(34600000008666666666666666666666666666666666666666666666666666666666666666666624378356234789568734563478564952348965347856478356234789563478256910786783562783457846534789560134756172568683465934785367460952375234785349568478932567863478562348756435789627836217867856487562347856437555514895623478563478569134785697835623784576978162568983475043838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838383838455555555555555555555555555555555555555555555555550**2)
print()
print()
print(3**2)
print(9**0.5)




